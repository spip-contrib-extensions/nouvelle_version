<?php

function formulaires_configversioning_charger_dist(){
	$workflow_init = lire_config('versioning/workflow');
	$valeurs = array('workflow'=>$workflow_init);
	return $valeurs;
}

function formulaires_configversioning_verifier_dist(){
    $erreurs = array();
    return $erreurs;
}

function formulaires_configversioning_traiter_dist(){
    $workflow_new = _request('workflow');
    ecrire_config("versioning/workflow", $workflow_new);
    return array('message_ok'=>_T('versioning:operation_executee')." Workflow ".$workflow_new);
}
