<?php
/***************************************************************************\
 * Plugin Nouvelle Version pour Spip 4.1
 * Licence GPL (c) 2011
 * Modération de la nouvelle version d'un article
 *
\***************************************************************************/

/**
 * Duplique un article dans la rubrique
 * - Conserve le contenu de l'article source
 * - Conserve les logos de l'article source
 * - Conserve le statut de publication de l'article source
 */
function dupliquer_article($article,$rubrique){
	include_spip('action/editer_article');
	//include_spip('inc/modifier_article');
	include_spip('inc/modifier');

	// On lit l'article qui va etre dupliqué
	$champs = array('*');
	$from = 'spip_articles';
	$where = array(
		"id_article=".$article
	);
	$infos = sql_allfetsel($champs, $from, $where);
	// On choisi les champs que l'on veut conserver
	// TODO éventuellement passer cette variable en CFG pour choisir depuis SPIP les champs à conserver ?
	$champs_dupliques = array(
		'surtitre','titre','soustitre','descriptif','chapo','texte','ps','accepter_forum','lang','langue_choisie','nom_site','url_site'
	);
	foreach ($champs_dupliques as $key => $value) {
		$infos_de_l_article[$value] = $infos[0][$value];
	}

	// On cherche ses mots clefs et les docs
	$mots_clefs_de_l_article = lire_les_mots_clefs($article,'article');
	$documents_de_l_article = lire_les_documents($article,'article');

	//////////////
	// ON DUPLIQUE
	//////////////
	// On le clone avec les champs choisis ci-dessus, il sera NON publié par défaut
	$id_article = article_inserer($rubrique);
	article_modifier($id_article, $infos_de_l_article);

	// On lui rend son statut
	//$maj_statut_article = sql_updateq("spip_articles", array('statut' => $infos[0]['statut']), "id_article=".$id_article);

	// On lui rend met version_of à 1
	$maj_statut_article = sql_updateq("spip_articles", array('version_of' => $article), "id_article=".$id_article);

	// On lui remet ses mots clefs et les docs
	remettre_les_mots_clefs($mots_clefs_de_l_article,$id_article,'article');
	remettre_les_documents($documents_de_l_article,$id_article,'article');

	// On lui copie ses logos
	dupliquer_logo($article,$id_article,'article',false);
	dupliquer_logo($article,$id_article,'article',true);

	return $id_article;
}


function lire_les_mots_clefs($id,$type){
	$mots_clefs = sql_allfetsel('id_mot', 'spip_mots_liens',  array('id_objet=' . intval($id) . ' AND objet=\''.$type.'\''));
	return $mots_clefs;
}

function remettre_les_mots_clefs($mots,$id,$type){
/* ON MET A BLANC POUR EVITER LES REDONDANCES A LA PUBLICATION*/
	$p = sql_delete("spip_mots_liens", "id_objet=$id AND objet='$type'");

	foreach($mots as $champ => $valeur){

	    $n = sql_insertq('spip_mots_liens', array(
	    'id_mot'=> $valeur['id_mot'],
	    'id_objet'=> $id,
	    'objet'=>$type
	    )
	);

	}

	return true;
}


function lire_les_documents($id,$type){
	$documents = sql_allfetsel('id_document', 'spip_documents_liens', array('id_objet=' . intval($id) . ' AND objet=\''.$type.'\''));
	return $documents;
}

function remettre_les_documents($documents,$id,$type){
	/* ON MET A BLANC POUR EVITER LES REDONDANCES A LA PUBLICATION*/
	$p = sql_delete("spip_documents_liens", "id_objet=$id AND objet='$type'");

	foreach($documents as $champ => $valeur){
	    $n = sql_insertq('spip_documents_liens', array(
	    'id_document'=> $valeur['id_document'],
	    'id_objet'=> $id,
	    'objet'=>$type
	    )
	);
	}

	return true;
}


/* FONCTION HONTEUSEMENT ADAPTEE DE DOCUCOPIEUR ==> https://contrib.spip.net/DocuCopieur */
/* cette fonction realise la copie d'un logo d'article et de son logo de survol */
/* vers le nouvel article. */
function dupliquer_logo($id_source, $id_destination, $type='article', $bsurvol = false ){

/****** NON FONCTIONNEL - A FAIRE *****/
	/****
	include_spip('action/iconifier');
	global $formats_logos;

	if ( $bsurvol == true )
	{
		$logo_type = 'off';	// logo survol
	} else  $logo_type = 'on';	// logo

	$chercher_logo = charger_fonction('chercher_logo', 'inc');

	$logo_source = $chercher_logo($id_source, 'id_'.$type, $logo_type );
	$logo_source = $logo_source[0];
	if ( !file_exists($logo_source) ) return false;

	$size = @getimagesize($logo_source);
	$mime = !$size ? '': $size['mime'];
	$source['name'] = basename($logo_source);
	$source['type'] = $mime;
	$source['tmp_name'] = $logo_source;
	$source['error'] = 0;
	$source['size'] = @filesize($logo_source);

	action_spip_image_ajouter_dist(substr($type,0,3). $logo_type .$id_destination, 'local', $source );
	****/
	return true;
}
