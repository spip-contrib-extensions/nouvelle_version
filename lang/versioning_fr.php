<?php
/***************************************************************************\
 * Plugin Nouvelle Version pour Spip 4.1
 * Licence GPL (c) 2011
 * Modération de la nouvelle version d'un article
 *
\***************************************************************************/

// Ceci est un fichier langue de SPIP  --  This is a SPIP language file
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'article_mots_dupliques_meme_rubrique' => "L'article et ses mots clefs seront dupliqués dans la même rubrique.",
	'attention' => "ATTENTION",

	// D
	'duplication_de_l_article' => "Duplication de l'article",
	'dupliquer_un_article' => "Dupliquer un article",

	// E
	'erreur_survenue' => "Une erreur est survenue.",

	// I
	'icone_dupliquer' => 'Dupliquer la rubrique',
	'icone_dupliquer_article' => "Proposer des modifications",
	'icone_menu_config' => 'Modération des modifications',
	'icone_remplacer_article' => "Publier cette version",

	// O
	'operation_annulee' => "L'opération a été annulée.",
	'operation_executee' => "L'opération a bien été exécutée.",
	'operation_retour_ko' => "Retour aux rubriques.",
	'operation_retour_ko_article' => "Retour aux articles.",
	'operation_retour_ok' => "Se rendre dans la rubrique copiée.",
	'operation_retour_ok_article' => "Se rendre sur l'article dupliqué.",
	'operation_retour_ok_article_publi' => "Se rendre sur l'article publié.",

	// M
	'message_annuler' => 'Annuler',
	'message_confirmer' => 'Confirmer',

	// P
	'publication_nouvelle_version_de_l_article' => "Publication d'une nouvelle version de l'article",
	'publication_nouvelle_version_d_un_article' => "Publication d'une nouvelle version d'un article",

	// R
	'refus_1' => "Vous n'avez pas l'autorisation d'accéder à cette page de configuration",
	'reglage_wokflow' => "Réglages du Workflow",

	// S
	'se_rendre_sur_la_version' => "Voir la version modifiée en cours d'édition",
	'se_rendre_sur_l_original' => "Voir la version publiée",

	// V
	'versioning_menu' => "Modération des modifications",
	'version_publiee_article_archive' => "La version sera publiée et l'article en ligne actuellement sera archivé.",

	// W
	'workflow' => "Processus de gestion des auteurs",
	'workflow1' => "<strong>Workflow 1</strong> : <i>Par défaut</i> L'auteur de la modification est le seul mentionné dans la liste des auteurs",
	'workflow2' => "<strong>Workflow 2</strong> : Les auteurs des modifications successives sont tous maintenus en auteurs de l'article"
);
