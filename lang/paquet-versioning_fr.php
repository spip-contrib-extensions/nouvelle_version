<?php

/***************************************************************************\
 * Plugin Nouvelle Version pour Spip 4.1
 * Licence GPL (c) 2011
 * Modération de la nouvelle version d'un article
 *
\***************************************************************************/

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// V
	'versioning_description' => 'Ce plugin permet de dupliquer un article et son contenu depuis l\'espace privé en ajoutant un bouton dans la navigation de gauche dans l\'espace privé, puis de le publier en lieu et place de l\'article original.
	l\'article original est alors archivé

	Cela permet en somme une modération des modifications sur un article publié (comme le permettait la fonctionnalité \'nouvelle version\' d\'Agora-SPIP)',
	'versioning_slogan' => 'Nouvelle version - Agora spirit',
);
